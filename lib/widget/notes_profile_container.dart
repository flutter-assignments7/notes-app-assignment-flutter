import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class NotesProfileContainer extends StatelessWidget {
  final String textTitle;
  final String number;

  NotesProfileContainer({
    required this.textTitle,
    required this.number,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: SizeConfig.scaleHeight(58),
      width: SizeConfig.scaleWidth(85),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
        border: Border.all(
          width: SizeConfig.scaleWidth(2),
          color: Color(0xFF6A90F2),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          NotesText(
            text: textTitle,
            fontSize: SizeConfig.scaleTextFont(12),
            fontWeight: FontWeight.w500,
            textColor: Color(0xFF6A90F2),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(5),
          ),
          NotesText(
            text: number,
            fontSize: SizeConfig.scaleTextFont(12),
            fontWeight: FontWeight.w500,
            textColor: Color(0xFFA5A5A5),
          ),
        ],
      ),
    );
  }
}
