import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class NotesContainerElevatedButton extends StatelessWidget {
  final double btnMarginTop;
  final double btnMarginBottom;
  final double btnMarginStart;
  final double btnMarginEnd;
  final VoidCallback? onPressed;
  final String btnText;

  NotesContainerElevatedButton({
    this.btnMarginTop = 0,
    this.btnMarginBottom = 0,
    this.btnMarginStart = 0,
    this.btnMarginEnd = 0,
    this.onPressed,
    required this.btnText,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: SizeConfig.scaleHeight(53),
      margin: EdgeInsetsDirectional.only(
        top: btnMarginTop,
        bottom: btnMarginBottom,
        start: btnMarginStart,
        end: btnMarginEnd,
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        child: NotesText(
          text: btnText,
          textColor: Colors.white,
          fontSize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.w500,
        ),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(26)),
          ),
          primary: Color(0xFF6A90F2),
        ),
      ),
    );
  }
}
