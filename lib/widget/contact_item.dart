import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class ContactItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final VoidCallback? onTapGoToCategory;
  final VoidCallback? onTapNewCategory;

  ContactItem({
    required this.title,
    required this.subtitle,
    this.onTapGoToCategory,
    this.onTapNewCategory,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(0xFF6A90F2),
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsetsDirectional.only(
        bottom: SizeConfig.scaleHeight(10),
      ),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
      ),
      child: Row(
        children: [
          Expanded(
            child: ListTile(
              onTap: onTapGoToCategory,
              tileColor: Colors.white,
              // contentPadding: EdgeInsetsDirectional.only(top: 11, bottom: 11, start: 15),
              leading: Container(
                width: SizeConfig.scaleWidth(48),
                height: SizeConfig.scaleHeight(48),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Color(0xFF6A90F2),
                  shape: BoxShape.circle,
                ),
                child: NotesText(
                  text: 'W',
                  fontSize: SizeConfig.scaleTextFont(22),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Quicksand',
                  textColor: Colors.white,
                ),
              ),
              title: NotesText(
                text: title,
                fontSize: SizeConfig.scaleTextFont(13),
                fontWeight: FontWeight.w500,
                fontFamily: 'Quicksand',
              ),
              subtitle: NotesText(
                text: subtitle,
                fontSize: SizeConfig.scaleTextFont(12),
                fontWeight: FontWeight.w500,
                fontFamily: 'Quicksand',
                textColor: Color(0xFFA5A5A5),
              ),
              trailing: Icon(
                Icons.delete,
                color: Color(0xFFD84040),
              ),
            ),
          ),
          InkWell(
            onTap: onTapNewCategory,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(5),
              ),
              child: Icon(
                Icons.edit,
                color: Colors.white,
                size: SizeConfig.scaleWidth(15),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
