import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Notes_Container_TextField extends StatelessWidget {
  // final double containerMarginTop;
  final TextEditingController? textEditingController;
  final double containerPaddingStart;
  final double containerPaddingEnd;
  final double containerPaddingTop;
  final double containerPaddingBottom;
  final double containerBorderRadiusTobStart;
  final double containerBorderRadiusTobEnd;
  final double containerBorderRadiusBottomStart;
  final double containerBorderRadiusBottomEnd;
  final String? textError;
  final bool obscureText;
  final TextInputType keyboardType;
  final String hintTextField;
  final double borderWidth;
  final double hintFontSize;
  final FontWeight hintFontWeight;
  final String hintFontFamily;
  final double textSize;

  Notes_Container_TextField({
    // this.containerMarginTop = 0,
    this.containerPaddingStart = 0,
    this.containerPaddingEnd = 0,
    this.containerPaddingTop = 0,
    this.containerPaddingBottom = 0,
    this.containerBorderRadiusTobStart = 0,
    this.containerBorderRadiusTobEnd = 0,
    this.containerBorderRadiusBottomEnd = 0,
    this.containerBorderRadiusBottomStart = 0,
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    required this.hintTextField,
    this.borderWidth = 1,
    this.hintFontSize = 22,
    this.hintFontWeight = FontWeight.w300,
    this.hintFontFamily = 'Roboto',
    this.textEditingController,
    this.textError,
    this.textSize = 22,

  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsetsDirectional.only(top: containerMarginTop),
      padding: EdgeInsetsDirectional.only(
        start: containerPaddingStart,
        end: containerPaddingEnd,
        top: containerPaddingTop,
        bottom: containerPaddingBottom,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadiusDirectional.only(
          topStart: Radius.circular(containerBorderRadiusTobStart),
          topEnd: Radius.circular(containerBorderRadiusTobEnd),
          bottomStart: Radius.circular(containerBorderRadiusBottomStart),
          bottomEnd: Radius.circular(containerBorderRadiusBottomEnd),
        ),
      ),

      child: TextField(
        controller: textEditingController,
        cursorColor: Colors.black,
        style: TextStyle(
          fontSize: textSize,
        ),
        cursorWidth: 1,
        cursorHeight: 30,
        obscureText: obscureText,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          errorText: textError,
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
          ),
          hintText: hintTextField,
          suffixText: 'Forget?',
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF9A9A9A),
              width: 1,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF9A9A9A),
              width: borderWidth,
            ),
          ),
          hintStyle: TextStyle(
            fontSize: hintFontSize,
            fontWeight: hintFontWeight,
            fontFamily: hintFontFamily,
            color: Color(0xFF9391A4),
          ),
        ),
      ),
    );
  }
}
