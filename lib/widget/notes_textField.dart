import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';

class NotesTextField extends StatelessWidget {
  final TextEditingController? textEditingController;
  final String? textError;
  final bool obscureText;
  final TextInputType keyboardType;
  final String hintTextField;
  final double borderWidth;
  final double hintFontSize;
  final FontWeight hintFontWeight;
  final String hintFontFamily;
  final double textSize;
  final double cursorHeight;


  NotesTextField({
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    required this.hintTextField,
    this.borderWidth = 1,
    this.hintFontSize = 22,
    this.hintFontWeight = FontWeight.w300,
    this.hintFontFamily = 'Roboto',
    this.textEditingController,
    this.textError,
    this.textSize = 22,
    this.cursorHeight = 30,

  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: textEditingController,
      cursorColor: Colors.black,
      style: TextStyle(
        fontSize: textSize,
      ),
      cursorWidth: SizeConfig.scaleWidth(1),
      cursorHeight: cursorHeight,
      obscureText: obscureText,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        errorText: textError,
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: SizeConfig.scaleWidth(1),
            color: Colors.red,
          ),
        ),
        hintText: hintTextField,
        /*suffixText: 'Forget?',
        suffixStyle: TextStyle(
          color: Colors.black
        ),*/
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFF9A9A9A),
            width: SizeConfig.scaleWidth(1),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFF9A9A9A),
            width: borderWidth,
          ),
        ),
        hintStyle: TextStyle(
          fontSize: hintFontSize,
          fontWeight: hintFontWeight,
          fontFamily: hintFontFamily,
          color: Color(0xFF9391A4),
        ),
      ),
    );
  }
}
