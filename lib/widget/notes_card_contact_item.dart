import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class NotesCardContactItem extends StatelessWidget {
  final String title;
  final String subTitle;

  NotesCardContactItem({
    required this.title,
    required this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      color: Color(0xFF6A90F2),
      margin: EdgeInsetsDirectional.only(
        bottom: SizeConfig.scaleHeight(10),
      ),
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(4),
          end: SizeConfig.scaleWidth(4),
        ),
        child: ListTile(
          tileColor: Colors.white,
          contentPadding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(18),
            bottom: SizeConfig.scaleHeight(18),
            start: SizeConfig.scaleWidth(16),
            end: SizeConfig.scaleWidth(16),
          ),
          title: NotesText(
            text: title,
            fontSize: SizeConfig.scaleTextFont(13),
            fontFamily: 'Quicksand',
            fontWeight: FontWeight.w500,
          ),
          subtitle: Container(
            margin: EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(43),
              top: SizeConfig.scaleHeight(4),
            ),
            child: NotesText(
              text: subTitle,
              fontSize: SizeConfig.scaleTextFont(12),
              fontWeight: FontWeight.w500,
              fontFamily: 'Quicksand',
              textColor: Color(0xFFA5A5A5),
            ),
          ),
          trailing: Container(
            height: double.infinity,
            child: Icon(
              Icons.check_circle,
              size: SizeConfig.scaleWidth(25),
              color: Color(0xFF98CE63),
            ),
          ),
        ),
      ),
    );
  }
}
