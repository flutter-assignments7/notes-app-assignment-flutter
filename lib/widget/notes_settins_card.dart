import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class NotesSettingsCard extends StatelessWidget {
  final double cardPaddingStart;
  final double cardPaddingEnd;
  final double contentPaddingStart;
  final double contentPaddingEnd;
  final IconData leadingIcon;
  final String title;
  final String subTitle;
  final VoidCallback? onTap;

  NotesSettingsCard({
    this.cardPaddingStart = 0,
    this.cardPaddingEnd = 0,
    this.contentPaddingStart = 15,
    this.contentPaddingEnd = 15,
    required this.leadingIcon,
    required this.title,
    required this.subTitle,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsetsDirectional.only(bottom: SizeConfig.scaleHeight(10)),
      clipBehavior: Clip.antiAlias,
      elevation: 3,
      color: Color(0xFF6A90F2),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.only(
            start: cardPaddingStart, end: cardPaddingEnd),
        child: ListTile(
          onTap: onTap,
          contentPadding: EdgeInsetsDirectional.only(
            start: contentPaddingStart,
            end: contentPaddingEnd,
            // top: SizeConfig.scaleHeight(11),
            // bottom: SizeConfig.scaleHeight(11),
          ),
          tileColor: Colors.white,
          leading: Container(
            height: SizeConfig.scaleHeight(48),
            width: SizeConfig.scaleWidth(48),
            decoration: BoxDecoration(
              color: Color(0xFF6A90F2),
              shape: BoxShape.circle,
            ),
            child: Icon(
              leadingIcon,
              size: SizeConfig.scaleWidth(20),
              color: Colors.white,
            ),
          ),
          title: NotesText(
            text: title,
            fontSize: SizeConfig.scaleTextFont(13),
            fontWeight: FontWeight.w500,
            fontFamily: 'Quicksand',
          ),
          subtitle: NotesText(
            text: subTitle,
            fontSize: SizeConfig.scaleTextFont(12),
            fontWeight: FontWeight.w500,
            fontFamily: 'Quicksand',
            textColor: Color(0xFFA5A5A5),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            color: Colors.black,
            size: SizeConfig.scaleWidth(24),
          ),
        ),
      ),
    );
  }
}
