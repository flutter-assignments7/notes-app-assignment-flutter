class Contact{
  String _name;
  String _details;

  Contact(this._name, this._details);

  String get details => _details;

  set details(String value) {
    _details = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }
}