import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_settins_card.dart';
import 'package:notes_app/widget/notes_text.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: NotesText(
          text: 'Settings',
          textColor: Color(0xFF474559),
          fontFamily: 'Quicksand',
          fontWeight: FontWeight.bold,
          fontSize: SizeConfig.scaleTextFont(22),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
              // Navigator.pushReplacementNamed(context, '/categories_screen');
            });
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
            size: SizeConfig.scaleWidth(24),
          ),
        ),
        // Icons.arrow_back_ios_outlined,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(18),
        ),
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(16),
                    bottom: SizeConfig.scaleHeight(8),
                  ),
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  decoration: BoxDecoration(
                    color: Color(0xFF6A90F2),
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        // color:
                        color: Colors.grey.withOpacity(0.5),
                        offset: Offset(0, 3),
                        blurRadius: 9,
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: NotesText(
                      text: 'IM',
                      fontSize: SizeConfig.scaleTextFont(24),
                      fontWeight: FontWeight.w400,
                      textColor: Colors.white,
                      fontFamily: 'Roboto Mono Regular',
                    ),
                  ),
                ),
                NotesText(
                  text: 'Ibrahim Mohammed',
                  fontSize: SizeConfig.scaleTextFont(15),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Quicksznd',
                ),
                NotesText(
                  text: 'Dev.IbrahimMH@gmail.com',
                  fontSize: SizeConfig.scaleTextFont(13),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Quicksznd',
                  textColor: Color(0xFFA5A5A5),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(10),
                ),
                Divider(
                  height: 0,
                  thickness: SizeConfig.scaleHeight(1),
                  indent: SizeConfig.scaleWidth(27),
                  endIndent: SizeConfig.scaleWidth(27),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(25),
                ),
                NotesSettingsCard(
                  leadingIcon: Icons.language,
                  title: 'Language',
                  subTitle: 'Selected language: EN',
                  cardPaddingStart: SizeConfig.scaleWidth(5),
                  contentPaddingStart: SizeConfig.scaleWidth(10),
                  contentPaddingEnd: SizeConfig.scaleWidth(15),
                ),
                NotesSettingsCard(
                  leadingIcon: Icons.perm_identity,
                  title: 'Profile',
                  subTitle: 'Update your data…',
                  cardPaddingEnd: SizeConfig.scaleWidth(5),
                  contentPaddingEnd: SizeConfig.scaleWidth(10),
                  contentPaddingStart: SizeConfig.scaleWidth(15),
                  onTap: () {
                    setState(() {
                      Navigator.pushNamed(context, '/profile_screen');
                    });
                  },
                ),
                NotesSettingsCard(
                  leadingIcon: Icons.perm_device_info,
                  title: 'About App',
                  subTitle: 'What is notes app?',
                  cardPaddingStart: SizeConfig.scaleWidth(5),
                  contentPaddingStart: SizeConfig.scaleWidth(10),
                  contentPaddingEnd: SizeConfig.scaleWidth(15),
                  onTap: () {
                    setState(() {
                      Navigator.pushNamed(context, '/about_app_screen');
                    });
                  },
                ),
                NotesSettingsCard(
                  leadingIcon: Icons.info,
                  title: 'About course',
                  subTitle: 'Describe the course in brief',
                  cardPaddingEnd: SizeConfig.scaleWidth(5),
                  contentPaddingEnd: SizeConfig.scaleWidth(10),
                  contentPaddingStart: SizeConfig.scaleWidth(15),
                ),
                NotesSettingsCard(
                  leadingIcon: Icons.power_settings_new,
                  title: 'Logout',
                  subTitle: 'Waiting your return…',
                  cardPaddingStart: SizeConfig.scaleWidth(5),
                  contentPaddingStart: SizeConfig.scaleWidth(10),
                  contentPaddingEnd: SizeConfig.scaleWidth(15),
                  onTap: () {
                    setState(() {
                      Navigator.pop(context);
                      Navigator.pushReplacementNamed(context, '/login_screen2');
                    });
                  },
                ),
              ],
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(10),
              left: 0,
              right: 0,
              child: NotesText(
                text: 'Flutter Course - Notes App V1.0',
                fontSize: SizeConfig.scaleTextFont(15),
                fontWeight: FontWeight.w300,
                textColor: Color(0xFF707070),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
