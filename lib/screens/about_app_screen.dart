import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class AboutAppScreen extends StatelessWidget {
  const AboutAppScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'images/launch_bg.png',
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: NotesText(
              text: 'About App',
              textColor: Color(0xFF474559),
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.bold,
              fontSize: SizeConfig.scaleTextFont(22),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.black,
              ),
            ),
          ),
          body: Stack(
            children: [
              Center(
                child: Container(
                  padding: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(29),
                  ),
                  width: SizeConfig.scaleWidth(250),
                  height: SizeConfig.scaleHeight(250),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
                    border: Border.all(
                      width: SizeConfig.scaleWidth(5),
                      color: Color(0xFF6A90F2),
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SvgPicture.asset(
                        'images/notes_logo1.svg',
                        width: SizeConfig.scaleWidth(120),
                        height: SizeConfig.scaleHeight(120),
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(5),
                      ),
                      NotesText(
                        text: 'My Notes',
                        fontSize: SizeConfig.scaleTextFont(30),
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Nunito',
                      ),
                      NotesText(
                        text: 'For Organized Life',
                        fontSize: SizeConfig.scaleTextFont(15),
                        fontWeight: FontWeight.w300,
                        textColor: Color(0xFF707070),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: SizeConfig.scaleHeight(10),
                left: 0,
                right: 0,
                child: NotesText(
                  text: 'Flutter Course - Notes App V1.0',
                  fontSize: SizeConfig.scaleTextFont(15),
                  fontWeight: FontWeight.w300,
                  textColor: Color(0xFF707070),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
