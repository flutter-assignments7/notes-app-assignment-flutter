import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_container_elevated_button.dart';
import 'package:notes_app/widget/notes_profile_container.dart';
import 'package:notes_app/widget/notes_text.dart';
import 'package:notes_app/widget/notes_textField.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: NotesText(
          text: 'Profile',
          textColor: Color(0xFF474559),
          fontFamily: 'Quicksand',
          fontWeight: FontWeight.bold,
          fontSize: SizeConfig.scaleTextFont(22),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
              // Navigator.pushReplacementNamed(context, '/categories_screen');
            });
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
            size: SizeConfig.scaleWidth(24),
          ),
        ),
        // Icons.arrow_back_ios_outlined,
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(start: 25, end: 25, top: 25),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
                ),
                child: ListTile(
                  leading: Container(
                    width: SizeConfig.scaleWidth(55),
                    height: SizeConfig.scaleHeight(55),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Color(0xFF6A90F2),
                      shape: BoxShape.circle,
                    ),
                    child: NotesText(
                      text: 'IM',
                      fontSize: SizeConfig.scaleTextFont(20),
                      fontWeight: FontWeight.w400,
                      textColor: Colors.white,
                      fontFamily: 'Roboto Mono Regular',
                    ),
                  ),
                  title: NotesText(
                    text: 'Ibrahim Mohammed',
                    fontSize: SizeConfig.scaleTextFont(13),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Quicksand',
                  ),
                  subtitle: NotesText(
                    text: 'Dev.IbrahimMH@gmaol.com',
                    fontSize: SizeConfig.scaleTextFont(12),
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Quicksand',
                    textColor: Color(0xFFA5A5A5),
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(15),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  NotesProfileContainer(textTitle: 'Categories', number: '14'),
                  NotesProfileContainer(textTitle: 'Done Notes', number: '14'),
                  NotesProfileContainer(textTitle: 'Waiting Notes', number: '14'),
                ],
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
                ),
                margin: EdgeInsetsDirectional.only(
                  top: SizeConfig.scaleHeight(15),
                  bottom: SizeConfig.scaleHeight(30),
                ),
                elevation: 4,
                child: Padding(
                  padding: EdgeInsetsDirectional.only(
                    top: SizeConfig.scaleHeight(10),
                    bottom: SizeConfig.scaleHeight(20),
                    start: SizeConfig.scaleWidth(20),
                    end: SizeConfig.scaleWidth(20),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      NotesTextField(
                        hintTextField: 'First Name',
                        hintFontSize: SizeConfig.scaleTextFont(14),
                        hintFontFamily: 'Quicksand',
                        hintFontWeight: FontWeight.w500,
                        textSize: SizeConfig.scaleTextFont(14),
                        borderWidth: SizeConfig.scaleWidth(1),
                        cursorHeight: SizeConfig.scaleHeight(20),
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      NotesTextField(
                        hintTextField: 'Last Name',
                        hintFontSize: SizeConfig.scaleTextFont(14),
                        hintFontFamily: 'Quicksand',
                        hintFontWeight: FontWeight.w500,
                        textSize: SizeConfig.scaleTextFont(14),
                        borderWidth: SizeConfig.scaleWidth(1),
                        cursorHeight: SizeConfig.scaleHeight(20),
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      NotesTextField(
                        hintTextField: 'Phone Number',
                        hintFontSize: SizeConfig.scaleTextFont(14),
                        hintFontFamily: 'Quicksand',
                        hintFontWeight: FontWeight.w500,
                        textSize: SizeConfig.scaleTextFont(14),
                        borderWidth: SizeConfig.scaleWidth(1),
                        cursorHeight: SizeConfig.scaleHeight(20),
                      ),
                      SizedBox(
                        height: SizeConfig.scaleHeight(10),
                      ),
                      NotesTextField(
                        hintTextField: 'Email Address',
                        hintFontSize: SizeConfig.scaleTextFont(14),
                        hintFontFamily: 'Quicksand',
                        hintFontWeight: FontWeight.w500,
                        textSize: SizeConfig.scaleTextFont(14),
                        borderWidth: SizeConfig.scaleWidth(1),
                        cursorHeight: SizeConfig.scaleHeight(20),
                      ),
                    ],
                  ),
                ),
              ),
              NotesContainerElevatedButton(
                onPressed: () {},
                btnText: 'Save',

              ),
            ],
          ),
        ),
      ),
    );
  }
}
