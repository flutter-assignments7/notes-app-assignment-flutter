import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_container_elevated_button.dart';
import 'package:notes_app/widget/notes_text.dart';
import 'package:notes_app/widget/notes_textField.dart';

class NewNotesScreen extends StatefulWidget {
  @override
  _NewNotesScreenState createState() => _NewNotesScreenState();
}

class _NewNotesScreenState extends State<NewNotesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
              // Navigator.pushReplacementNamed(context, '/category_Notes_screen');
            });
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
          ),
        ),
        // Icons.arrow_back_ios_outlined,
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(2),
          start: SizeConfig.scaleWidth(25),
          end: SizeConfig.scaleWidth(25),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            NotesText(
              text: 'New Note',
              fontSize: SizeConfig.scaleTextFont(30),
              fontFamily: 'Nunito',
              fontWeight: FontWeight.bold,
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(5),
            ),
            NotesText(
              text: 'Create note',
              fontSize: SizeConfig.scaleTextFont(18),
              fontWeight: FontWeight.w300,
              textColor: Color(0xFF9391A4),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(42),
            ),
            NotesTextField(
              hintTextField: 'Note Tile',
              textSize: SizeConfig.scaleTextFont(22),
              hintFontSize: SizeConfig.scaleTextFont(22),
              borderWidth: SizeConfig.scaleWidth(1),
              cursorHeight: SizeConfig.scaleHeight(30),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(20),
            ),
            NotesTextField(
              hintTextField: 'Description',
              textSize: SizeConfig.scaleTextFont(22),
              hintFontSize: SizeConfig.scaleTextFont(22),
              borderWidth: SizeConfig.scaleWidth(1),
              cursorHeight: SizeConfig.scaleHeight(30),
            ),
            NotesContainerElevatedButton(
              btnText: 'Save',
              btnMarginEnd: SizeConfig.scaleWidth(5),
              btnMarginStart: SizeConfig.scaleWidth(5),
              btnMarginTop: SizeConfig.scaleHeight(35),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
