import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:notes_app/widget/notes_container_textField.dart';
import 'package:notes_app/widget/notes_text.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;

  String? _emailError;
  String? _passwordError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tapGestureRecognizer = TapGestureRecognizer()..onTap = navigateToRegister;

    // _emailTextEditingController = TextEditingController(text:''); // لو في عندنا قيما بدائية بدنا نضيفها وتكون موجودة بحطها هان

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  void navigateToRegister() => Navigator.pushNamed(context, '/register_screen');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'images/launch_bg.png',
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(27.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsetsDirectional.only(top: 80),
                    child: NotesText(
                      text: 'Sign In',
                      fontSize: 30,
                      fontFamily: 'Nunito',
                      fontWeight: FontWeight.bold,
                      textColor: Color(0xFF23203F),
                    ),
                  ),
                  NotesText(
                    text: 'Login to start using app,',
                    fontSize: 18,
                    textColor: Color(0xFF707070),
                    fontWeight: FontWeight.w300,
                  ),
                  Container(
                    margin: EdgeInsetsDirectional.only(top: 80, bottom: 30),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          // color:
                          color: Colors.grey.withOpacity(0.16),
                          offset: Offset(-3, 3),
                          blurRadius: 7,
                          spreadRadius: 3,
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Notes_Container_TextField(
                          textEditingController: _emailTextEditingController,
                          keyboardType: TextInputType.emailAddress,
                          hintTextField: 'Email',
                          // containerMarginTop: 80,
                          containerPaddingBottom: 15,
                          containerPaddingEnd: 20,
                          containerPaddingStart: 20,
                          containerPaddingTop: 30,
                          containerBorderRadiusTobStart: 15,
                          containerBorderRadiusTobEnd: 15,
                          textError: _emailError,
                        ),
                        Notes_Container_TextField(
                          textEditingController: _passwordTextEditingController,
                          hintTextField: 'Password',
                          containerPaddingTop: 15,
                          containerPaddingStart: 20,
                          containerPaddingEnd: 20,
                          containerPaddingBottom: 30,
                          containerBorderRadiusBottomEnd: 15,
                          containerBorderRadiusBottomStart: 15,
                          obscureText: true,
                          keyboardType: TextInputType.visiblePassword,
                          textError: _passwordError,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 53,
                    margin: EdgeInsetsDirectional.only(bottom: 10),
                    child: ElevatedButton(
                      onPressed: () {
                        performLogin();
                      },
                      child: NotesText(
                        text: 'Login',
                        textColor: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(26),
                        ),
                        primary: Color(0xFF6A90F2),
                      ),
                    ),
                  ),
                  Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: 'Don\’t have an account? ',
                        style: TextStyle(
                          color: Color(0xFF9391A4),
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Roboto',
                          fontSize: 18,
                        ),
                        children: [
                          TextSpan(
                            recognizer: _tapGestureRecognizer,
                            text: 'SignUp',
                            style: TextStyle(
                              color: Color(0xFF23203F),
                              fontSize: 18,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void performLogin(){
    if(checkData()){
      //TODO:NAVIGATE TO HOME SCREEN
    }
  }

  //TODO:CHECK IF FIELDS HAVE DATA
  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty) {
      checkFieldsError();
      showMessage('Login Success');
      return true;
    }
    checkFieldsError();
    showMessage('Please, Enter Email or Password', error: true);
    return false;
  }

  void checkFieldsError(){
    setState(() {
      _emailError = _emailTextEditingController.text.isEmpty ? 'Enter Email Address' : null;
      _passwordError = _passwordTextEditingController.text.isEmpty ? 'Enter Email Address' : null;
    });
  }

//TODO:SHOW ERROR MESSAGE IN CASE OF ERROR DATA
  void showMessage(String message, {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        content: NotesText(
          text: message,
          textColor: Colors.white,
        ),
        backgroundColor: error ? Colors.red : Colors.green,
      ),
    );
  }

}
