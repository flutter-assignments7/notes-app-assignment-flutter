import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_container_elevated_button.dart';
import 'package:notes_app/widget/notes_container_textField.dart';
import 'package:notes_app/widget/notes_text.dart';
import 'package:notes_app/widget/notes_textField.dart';

class LoginScreen2 extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen2> {
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;

  String? _emailError;
  String? _passwordError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tapGestureRecognizer = TapGestureRecognizer()..onTap = navigateToRegister;

    // _emailTextEditingController = TextEditingController(text:''); // لو في عندنا قيما بدائية بدنا نضيفها وتكون موجودة بحطها هان

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }

  void navigateToRegister() =>
      Navigator.pushNamed(context, '/register_screen2');

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'images/launch_bg.png',
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(27),
                start: SizeConfig.scaleWidth(27),
                end: SizeConfig.scaleWidth(27),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: SizeConfig.scaleHeight(80),
                  ),
                  NotesText(
                    text: 'Sign In',
                    fontSize: SizeConfig.scaleTextFont(30),
                    fontFamily: 'Nunito',
                    fontWeight: FontWeight.bold,
                    textColor: Color(0xFF23203F),
                  ),
                  NotesText(
                    text: 'Login to start using app,',
                    fontSize: SizeConfig.scaleTextFont(18),
                    textColor: Color(0xFF707070),
                    fontWeight: FontWeight.w300,
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
                    ),
                    margin: EdgeInsetsDirectional.only(
                      top: SizeConfig.scaleHeight(80),
                      bottom: SizeConfig.scaleHeight(30),
                    ),
                    elevation: 4,
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(
                        top: SizeConfig.scaleHeight(30),
                        bottom: SizeConfig.scaleHeight(30),
                        start: SizeConfig.scaleWidth(20),
                        end: SizeConfig.scaleWidth(20),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          NotesTextField(
                            textEditingController: _emailTextEditingController,
                            keyboardType: TextInputType.emailAddress,
                            hintTextField: 'Email',
                            textError: _emailError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(30),
                          ),
                          NotesTextField(
                            textEditingController:
                                _passwordTextEditingController,
                            hintTextField: 'Password',
                            obscureText: true,
                            keyboardType: TextInputType.visiblePassword,
                            textError: _passwordError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                        ],
                      ),
                    ),
                  ),
                  NotesContainerElevatedButton(
                    btnText: 'Login',
                    btnMarginBottom: SizeConfig.scaleHeight(10),
                    onPressed: () {
                      performLogin();
                    },
                  ),
                  Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: 'Don\’t have an account? ',
                        style: TextStyle(
                          color: Color(0xFF9391A4),
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Roboto',
                          fontSize: SizeConfig.scaleTextFont(18),
                        ),
                        children: [
                          TextSpan(
                            recognizer: _tapGestureRecognizer,
                            text: 'SignUp',
                            style: TextStyle(
                              color: Color(0xFF23203F),
                              fontSize: SizeConfig.scaleTextFont(18),
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void performLogin() {
    if (checkData()) {
      //TODO:NAVIGATE TO HOME SCREEN
    }
  }

  //TODO:CHECK IF FIELDS HAVE DATA
  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty) {
      checkFieldsError();
      showMessage('Login Success');
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, '/categories_screen');
      });
      return true;
    }
    checkFieldsError();
    showMessage('Please, Enter required data', error: true);
    return false;
  }

  void checkFieldsError() {
    setState(() {
      _emailError = _emailTextEditingController.text.isEmpty
          ? 'Enter Email Address'
          : null;
      _passwordError =
          _passwordTextEditingController.text.isEmpty ? 'Enter Password' : null;
    });
  }

//TODO:SHOW ERROR MESSAGE IN CASE OF ERROR DATA
  void showMessage(String message, {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        content: NotesText(
          text: message,
          textColor: Colors.white,
        ),
        backgroundColor: error ? Colors.red : Colors.green,
      ),
    );
  }
}
