import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notes_app/models/contact.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/contact_item.dart';
import 'package:notes_app/widget/notes_text.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  List<Contact> _contacts = <Contact>[
    Contact('Work', 'Notes for work...'),
    Contact('Work', 'Notes for work...'),
    Contact('Work', 'Notes for work...'),
    Contact('Work', 'Notes for work...'),
    Contact('Work', 'Notes for work...'),
    Contact('Work', 'Notes for work...'),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                Navigator.pushNamed(context, '/settings_screen');
              });
            },
            icon: Icon(
              Icons.settings,
              color: Colors.black,
            ),
          ),
        ],
        title: NotesText(
          text: 'Categories',
          textColor: Color(0xFF474559),
          fontFamily: 'Quicksand',
          fontWeight: FontWeight.bold,
          fontSize: SizeConfig.scaleTextFont(22),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(18),
        ),
        child: Stack(
          children: [
            ListView.builder(
              padding: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(16),
              ),
              itemBuilder: (context, index) {
                Contact contact = _contacts.elementAt(index);
                return ContactItem(
                  title: contact.name,
                  subtitle: contact.details,
                  onTapGoToCategory: () {
                    setState(() {
                      Navigator.pushNamed(context, '/category_Notes_screen');
                    });
                  },

                  onTapNewCategory: () {
                    setState(() {
                      Navigator.pushNamed(context, '/new_category_screen');
                    });
                  },

                  // title: _contacts.elementAt(index).name,
                  // subtitle: _contacts.elementAt(index).details,
                );
              },
              itemCount: _contacts.length,
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(23),
              right: SizeConfig.scaleWidth(5),
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, '/new_category_screen');
                },
                child: CircleAvatar(
                  radius: SizeConfig.scaleWidth(30),
                  backgroundColor: Color(0xFF6A90F2),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: SizeConfig.scaleWidth(30),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
