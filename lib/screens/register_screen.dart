// import 'package:flutter/material.dart';
// import 'package:notes_app/widget/notes_container_textField.dart';
// import 'package:notes_app/widget/notes_text.dart';
//
// class RegisterScreen extends StatefulWidget {
//   const RegisterScreen({Key? key}) : super(key: key);
//
//   @override
//   _RegisterScreenState createState() => _RegisterScreenState();
// }
//
// class _RegisterScreenState extends State<RegisterScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         Image.asset(
//           'images/launch_bg.png',
//           height: double.infinity,
//           width: double.infinity,
//           fit: BoxFit.cover,
//         ),
//         Scaffold(
//           backgroundColor: Colors.transparent,
//           appBar: AppBar(
//             backgroundColor: Colors.transparent,
//             elevation: 0,
//             // iconTheme: IconThemeData(
//             //   color: Colors.black,
//             // ),
//             leading: IconButton(
//               onPressed: () {
//                 setState(() {
//                   Navigator.pushReplacementNamed(context, '/login_screen');
//                 });
//               },
//               icon: Icon(
//                 Icons.arrow_back_ios_outlined,
//                 color: Colors.black,
//               ),
//             ),
//             // Icons.arrow_back_ios_outlined,
//           ),
//           body: SingleChildScrollView(
//             child: Padding(
//               padding: const EdgeInsetsDirectional.only(start: 27, end: 27),
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 children: [
//                   Container(
//                     margin: EdgeInsetsDirectional.only(top: 0),
//                     child: NotesText(
//                       text: 'Sign In',
//                       fontSize: 30,
//                       fontFamily: 'Nunito',
//                       fontWeight: FontWeight.bold,
//                       textColor: Color(0xFF23203F),
//                     ),
//                   ),
//                   NotesText(
//                     text: 'Login to start using app,',
//                     fontSize: 18,
//                     textColor: Color(0xFF707070),
//                     fontWeight: FontWeight.w300,
//                   ),
//                   Container(
//                     margin: EdgeInsetsDirectional.only(top: 80, bottom: 30),
//                     decoration: BoxDecoration(
//                       boxShadow: [
//                         BoxShadow(
//                           // color:
//                           color: Colors.grey.withOpacity(0.16),
//                           offset: Offset(-3, 3),
//                           blurRadius: 7,
//                           spreadRadius: 3,
//                         ),
//                       ],
//                     ),
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       children: [
//                         Notes_Container_TextField(
//                           hintTextField: 'First name',
//                           // containerMarginTop: 80,
//                           containerPaddingBottom: 7,
//                           containerPaddingEnd: 20,
//                           containerPaddingStart: 20,
//                           containerPaddingTop: 30,
//                           containerBorderRadiusTobStart: 10,
//                           containerBorderRadiusTobEnd: 10,
//                         ),
//                         Notes_Container_TextField(
//                           hintTextField: 'Last name',
//                           containerPaddingTop: 7,
//                           containerPaddingStart: 20,
//                           containerPaddingEnd: 20,
//                           containerPaddingBottom: 7,
//                           obscureText: true,
//                         ),
//                         Notes_Container_TextField(
//                           hintTextField: 'Email',
//                           keyboardType: TextInputType.emailAddress,
//                           containerPaddingTop: 7,
//                           containerPaddingStart: 20,
//                           containerPaddingEnd: 20,
//                           containerPaddingBottom: 7,
//                           obscureText: true,
//                         ),
//                         Notes_Container_TextField(
//                           hintTextField: 'Phone',
//                           keyboardType: TextInputType.phone,
//                           containerPaddingTop: 7,
//                           containerPaddingStart: 20,
//                           containerPaddingEnd: 20,
//                           containerPaddingBottom: 7,
//                           obscureText: true,
//                         ),
//                         Notes_Container_TextField(
//                           hintTextField: 'Password',
//                           containerPaddingTop: 7,
//                           containerPaddingStart: 20,
//                           containerPaddingEnd: 20,
//                           containerPaddingBottom: 30,
//                           containerBorderRadiusBottomEnd: 10,
//                           containerBorderRadiusBottomStart: 10,
//                           obscureText: true,
//                         ),
//                       ],
//                     ),
//                   ),
//                   Container(
//                     width: double.infinity,
//                     height: 53,
//                     // margin: EdgeInsetsDirectional.only(bottom: 10),
//                     child: ElevatedButton(
//                       onPressed: () {},
//                       child: NotesText(
//                         text: 'Login',
//                         textColor: Colors.white,
//                         fontSize: 20,
//                         fontWeight: FontWeight.w500,
//                       ),
//                       style: ElevatedButton.styleFrom(
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(26),
//                         ),
//                         primary: Color(0xFF6A90F2),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }
