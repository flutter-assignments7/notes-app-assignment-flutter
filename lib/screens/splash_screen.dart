import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_text.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, '/login_screen2');
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Theme.of(context).disabledColor,
        ),
        child: Stack(
          children: [
            Image.asset(
              'images/launch_bg.png',
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(
                    'images/notes_logo1.svg',
                    width: SizeConfig.scaleWidth(120),
                    height: SizeConfig.scaleHeight(120),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(5),),
                  NotesText(
                    text: 'My Notes',
                    fontSize: SizeConfig.scaleTextFont(30),
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Nunito',
                  ),
                  NotesText(
                    text: 'For Organized Life',
                    fontSize: SizeConfig.scaleTextFont(15),
                    fontWeight: FontWeight.w300,
                    textColor: Color(0xFF707070),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(10),
              left: 0,
              right: 0,
              child: NotesText(
                text: 'Flutter Course - Notes App V1.0',
                fontSize: SizeConfig.scaleTextFont(15),
                fontWeight: FontWeight.w300,
                textColor: Color(0xFF707070),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
