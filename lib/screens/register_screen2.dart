import 'package:flutter/material.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_container_elevated_button.dart';
import 'package:notes_app/widget/notes_container_textField.dart';
import 'package:notes_app/widget/notes_text.dart';
import 'package:notes_app/widget/notes_textField.dart';

class RegisterScreen2 extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen2> {
  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;
  late TextEditingController _firstNameTextEditingController;
  late TextEditingController _lastNameTextEditingController;
  late TextEditingController _phoneTextEditingController;

  String? _emailError;
  String? _passwordError;
  String? _phoneError;
  String? _firstNameError;
  String? _lastNameError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
    _firstNameTextEditingController = TextEditingController();
    _lastNameTextEditingController = TextEditingController();
    _phoneTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
    _firstNameTextEditingController = TextEditingController();
    _lastNameTextEditingController = TextEditingController();
    _phoneTextEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'images/launch_bg.png',
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            // iconTheme: IconThemeData(
            //   color: Colors.black,
            // ),
            leading: IconButton(
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                  // Navigator.pushReplacementNamed(context, '/login_screen2');
                });
              },
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.black,
              ),
            ),

          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsetsDirectional.only(
                start: SizeConfig.scaleWidth(27),
                end: SizeConfig.scaleWidth(27),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  NotesText(
                    text: 'Sign Up',
                    fontSize: SizeConfig.scaleTextFont(22),
                    fontFamily: 'Nunito',
                    fontWeight: FontWeight.bold,
                    textColor: Color(0xFF23203F),
                  ),
                  NotesText(
                    text: 'Create an account',
                    fontSize: SizeConfig.scaleTextFont(18),
                    textColor: Color(0xFF707070),
                    fontWeight: FontWeight.w300,
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(SizeConfig.scaleWidth(10)),
                    ),
                    margin: EdgeInsetsDirectional.only(
                      top: SizeConfig.scaleHeight(80),
                      bottom: SizeConfig.scaleHeight(30),
                    ),
                    elevation: 4,
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(
                        top: SizeConfig.scaleHeight(32),
                        bottom: SizeConfig.scaleHeight(32),
                        start: SizeConfig.scaleWidth(15),
                        end: SizeConfig.scaleWidth(15),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          NotesTextField(
                            textEditingController:
                                _firstNameTextEditingController,
                            hintTextField: 'First name',
                            textError: _firstNameError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(14),
                          ),
                          NotesTextField(
                            textEditingController:
                                _lastNameTextEditingController,
                            hintTextField: 'Last name',
                            textError: _lastNameError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          NotesTextField(
                            textEditingController: _emailTextEditingController,
                            hintTextField: 'Email',
                            keyboardType: TextInputType.emailAddress,
                            textError: _emailError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          NotesTextField(
                            textEditingController: _phoneTextEditingController,
                            hintTextField: 'Phone',
                            keyboardType: TextInputType.phone,
                            textError: _phoneError,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                          SizedBox(
                            height: SizeConfig.scaleHeight(18),
                          ),
                          NotesTextField(
                            textEditingController:
                                _passwordTextEditingController,
                            hintTextField: 'Password',
                            textError: _passwordError,
                            obscureText: true,
                            textSize: SizeConfig.scaleTextFont(22),
                            hintFontSize: SizeConfig.scaleTextFont(22),
                            borderWidth: SizeConfig.scaleWidth(1),
                            cursorHeight: SizeConfig.scaleHeight(30),
                          ),
                        ],
                      ),
                    ),
                  ),
                  NotesContainerElevatedButton(
                    btnText: 'Sign Up',
                    onPressed: () {
                      performLogin();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void performLogin() {
    if (checkData()) {
      //TODO:NAVIGATE TO HOME SCREEN
    }
  }

  //TODO:CHECK IF FIELDS HAVE DATA
  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty &&
        _firstNameTextEditingController.text.isNotEmpty &&
        _lastNameTextEditingController.text.isNotEmpty &&
        _phoneTextEditingController.text.isNotEmpty) {
      checkFieldsError();
      showMessage('Register Success');
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, '/categories_screen');
      });
      return true;
    }
    checkFieldsError();
    showMessage('Please, Enter required data', error: true);
    return false;
  }

  void checkFieldsError() {
    setState(() {
      _emailError = _emailTextEditingController.text.isEmpty
          ? 'Enter Email Address'
          : null;
      _passwordError =
          _passwordTextEditingController.text.isEmpty ? 'Enter Password' : null;
      _firstNameError = _firstNameTextEditingController.text.isEmpty
          ? 'Enter First Name'
          : null;
      _lastNameError = _lastNameTextEditingController.text.isEmpty
          ? 'Enter Last Name'
          : null;
      _phoneError = _phoneTextEditingController.text.isEmpty
          ? 'Enter Phone Number'
          : null;
    });
  }

//TODO:SHOW ERROR MESSAGE IN CASE OF ERROR DATA
  void showMessage(String message, {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(25)),
        ),
        content: NotesText(
          text: message,
          textColor: Colors.white,
        ),
        backgroundColor: error ? Colors.red : Colors.green,
      ),
    );
  }
}
