import 'package:flutter/material.dart';
import 'package:notes_app/models/contact.dart';
import 'package:notes_app/utils/size_config.dart';
import 'package:notes_app/widget/notes_card_contact_item.dart';
import 'package:notes_app/widget/notes_text.dart';

class CategoryNotesScreen extends StatefulWidget {
  const CategoryNotesScreen({Key? key}) : super(key: key);

  @override
  _CategoryNotesScreenState createState() => _CategoryNotesScreenState();
}

class _CategoryNotesScreenState extends State<CategoryNotesScreen> {
  List<Contact> _contacts = <Contact>[
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
    Contact(
      'Note Tile',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s,',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: NotesText(
          text: 'Category Name',
          textColor: Color(0xFF474559),
          fontFamily: 'Quicksand',
          fontWeight: FontWeight.bold,
          fontSize: SizeConfig.scaleTextFont(22),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                Navigator.pushNamed(context, '/new_notes_screen');
              });
            },
            icon: Icon(
              Icons.add_circle,
              color: Colors.black,
              size: SizeConfig.scaleWidth(24),
            ),
          ),
        ],
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
              // Navigator.pushReplacementNamed(context, '/categories_screen');
            });
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
            size: SizeConfig.scaleWidth(24),
          ),
        ),
        // Icons.arrow_back_ios_outlined,
      ),
      body: ListView.builder(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(17),
          start: SizeConfig.scaleWidth(18),
          end: SizeConfig.scaleWidth(18),
        ),
        itemCount: _contacts.length,
        itemBuilder: (context, index) {
          Contact contact = _contacts.elementAt(index);
          return NotesCardContactItem(
            title: contact.name,
            subTitle: contact.details,
          );
        },
      ),
    );
  }
}
