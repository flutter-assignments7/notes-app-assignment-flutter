import 'package:flutter/material.dart';
import 'package:notes_app/screens/about_app_screen.dart';
import 'package:notes_app/screens/categories_screen.dart';
import 'package:notes_app/screens/category_notes_screen.dart';
import 'package:notes_app/screens/login_screen.dart';
import 'package:notes_app/screens/login_screen2.dart';
import 'package:notes_app/screens/new_category_screen.dart';
import 'package:notes_app/screens/new_notes_screen.dart';
import 'package:notes_app/screens/profile_screen.dart';
import 'package:notes_app/screens/register_screen.dart';
import 'package:notes_app/screens/register_screen2.dart';
import 'package:notes_app/screens/settings_screen.dart';
import 'package:notes_app/screens/splash_screen.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute:'/splash_screen',

      routes: {
        '/splash_screen': (context) => SplashScreen(),
        // '/login_screen': (context) => LoginScreen(),
        '/login_screen2': (context) => LoginScreen2(),
        // '/register_screen': (context) => RegisterScreen(),
        '/register_screen2': (context) => RegisterScreen2(),
        '/categories_screen': (context) => CategoriesScreen(),
        '/new_category_screen': (context) => NewCategoryScreen(),
        '/category_Notes_screen': (context) => CategoryNotesScreen(),
        '/new_notes_screen': (context) => NewNotesScreen(),
        '/settings_screen': (context) => SettingsScreen(),
        '/about_app_screen': (context) => AboutAppScreen(),
        '/profile_screen': (context) => ProfileScreen(),
      },
    );
  }
}
